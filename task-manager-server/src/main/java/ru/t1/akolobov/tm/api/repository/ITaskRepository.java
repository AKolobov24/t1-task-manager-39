package ru.t1.akolobov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Update("CREATE TABLE IF NOT EXISTS tm_task " +
            "( " +
            "id character(36) PRIMARY KEY," +
            "created timestamp," +
            "user_id character(36) REFERENCES tm_user (id)," +
            "name varchar(255)," +
            "description text," +
            "status varchar(255)," +
            "project_id character(36) REFERENCES tm_project" +
            ");"
    )
    void checkDatabaseTable();

    @Insert("INSERT INTO tm_task " +
            "(id, created, name, description, status, user_id, project_id) " +
            "VALUES(#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})"
    )
    void add(@NotNull Task model);

    @Update("UPDATE tm_task " +
            "SET name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}, " +
            "user_id = #{userId}, " +
            "project_id = #{projectId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull Task model);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_task")
    void clearAll();

    @Select("SELECT count(*) > 0 FROM tm_task " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1"
    )
    boolean existById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY ${sortColumn}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByUserIdSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sortColumn") String sortColumn
    );

    @Select("SELECT * FROM tm_task " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT count(*) FROM tm_task")
    @NotNull Integer getSize();

    @Select("SELECT count(*) FROM tm_task WHERE user_id = #{userId}")
    @NotNull Integer getSizeByUserId(@NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull Task model);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_task " +
            "WHERE user_id = #{userId} " +
            "AND project_id = #{projectId}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

}
