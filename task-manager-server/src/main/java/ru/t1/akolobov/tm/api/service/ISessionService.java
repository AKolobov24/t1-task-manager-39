package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.Session;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ISessionService {

    @NotNull Session add(@Nullable Session session);

    @NotNull Session add(@Nullable String userId, @Nullable Session session);

    @NotNull Collection<Session> add(@NotNull Collection<Session> models);

    @NotNull Session update(@Nullable String userId, @Nullable Session session);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull List<Session> findAll(@Nullable String userId);

    @NotNull List<Session> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable Session findOneById(@Nullable String userId, @Nullable String id);

    @NotNull Integer getSize(@Nullable String userId);

    @NotNull Session remove(@Nullable String userId, @Nullable Session session);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Session updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Date date,
                       @Nullable Role role);

}
